import numpy as np
import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F

from collections import OrderedDict 
from collections import deque

import copy
import gym
import gym.spaces

np.random.seed(0)
torch.random.manual_seed(0)

# if we can use gpu, set device cuda:0
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")

print(device)

env = gym.make("CartPole-v0")
num_act = 2
npop = 50
sigma = 0.05
alpha = 0.5

# Model
class actor(nn.Module):
    def __init__(self, name):
        super(actor, self).__init__()
        self.name = name
        self.head1 = nn.Linear(4, 60, bias=False)
        init.kaiming_normal_(self.head1.weight, nonlinearity="relu")
        
        self.head2 = nn.Linear(60, 40, bias=False) 
        init.kaiming_normal_(self.head2.weight, nonlinearity="relu")
        
        self.head3 = nn.Linear(40, num_act, bias=False) # softmax
        init.kaiming_normal_(self.head3.weight, nonlinearity="relu")

    def forward(self, x):
        x = F.relu(self.head1(x))
        x = F.relu(self.head2(x))
        x = F.softmax(self.head3(x), dim=1)
        return x

actor = actor(name = 'actor').to(device)
actor_copy = copy.deepcopy(actor)

num_episode = 200
model_keys = actor.state_dict().keys()
max_prob = deque(maxlen=100)

for i_ep in range(num_episode):
    N = [OrderedDict({key : param + sigma * torch.rand_like(param) for key, param in zip(model_keys, actor.parameters())}) for _ in range(npop)]
    R = np.zeros(npop)
    for i, p_ in enumerate(N):
        counts = 0
        init_state = env.reset()
        done = False
        actor.load_state_dict(p_)
        ep_reward = 0
        while not done:
            state = init_state.copy() if counts == 0 else state
            probs = actor.forward(torch.from_numpy(state.reshape(1,-1).astype(np.float32)).to(device))            
            action = np.random.choice(np.arange(0, 2), size=1, p=np.float32(probs.cpu().data.numpy()[0, :]))[0]
            next_frame, reward, done, _ = env.step(action)
            state = next_frame
            ep_reward += reward
            counts += 1
            max_prob.append(probs.cpu().data.numpy())
        R[i] = ep_reward

    A = (R - R.mean()) / (R.std() + 1e-7)
    probs = np.vstack(max_prob)

    params_list = [[] for _ in range(len(model_keys))]

    for i in range(npop):
        for j, key in enumerate(model_keys):
            params_list[j].append(N[i][key].unsqueeze(0) * A[i])

    for k, param in enumerate(actor_copy.parameters()):
        param.data.add_(alpha / (npop * sigma) * torch.cat(params_list[k], dim=0).sum(dim=0))

    actor.load_state_dict(actor_copy.state_dict())
    print("Episode {}, Min Score : {} Max Score : {} Max Prob {} ".format(i_ep, R.min(), R.max(), probs.max()))

# Testing
print("\n\nStart Testing !!")
is_render = True
for i_ep in range(100):
    counts = 0
    init_state = env.reset()
    done = False
    ep_reward = 0
    while not done:
        state = init_state.copy() if counts == 0 else state
        probs = actor.forward(torch.from_numpy(state.reshape(1,-1).astype(np.float32)).to(device))            
        action = np.random.choice(np.arange(0, 2), size=1, p=np.float32(probs.cpu().data.numpy()[0, :]))[0]
        if is_render:
            env.render()
        next_frame, reward, done, _ = env.step(action)
        state = next_frame
        ep_reward += reward
        counts += 1
    print("Episode {}, Score : {} ".format(i_ep, ep_reward))
