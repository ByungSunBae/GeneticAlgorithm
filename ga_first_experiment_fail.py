# This code is about simple genetic algorithm
# the basic process for a genetic algorithm
# 1) Initialization
# 2) Evaluation
# 3) Selection
# 4) Crossover
# 5) Mutation
# 6) Repeat

import random
import numpy as np
import string
import operator

# generate target
target = "MANNER MAKES MAN"
print("Target is %s" % (target))

str_alphabet = ' ' + string.ascii_uppercase
str_alphabet_list = list(str_alphabet)
target_len = len(target)

def fitness_func(target, candidate):
    """Compute fitness for genetic algorithm

    Args:
        target (string): thing that we want to know.
        candidate (string): generated word.

    Returns:
        float: score to match target and candidate.

    Raises:
        AssertionError: len(target) equal to len(candidate)
    """
    assert len(target) == len(candidate)
    target_list = list(target)
    candidate_list = list(candidate)
    score_ = [np.float32(t_ == c_) for t_, c_ in zip(target_list, candidate_list)]
    score_ = np.array(score_)
    score = np.mean(score_)
    return score

# Start !
# Generate the initial population
num_population = 2000
population = [''.join(np.random.choice(str_alphabet_list, len(target)).tolist()) for _ in range(num_population)]

# Compute fitness and sorting
scores_dict = {candidate : fitness_func(target, candidate)  for candidate in population}
sorted_scores_dict = sorted(scores_dict.items(), key=operator.itemgetter(1), reverse=True)
all_idx = np.arange(0, target_len)

# Setting hyperparameters
prob_crossover = 0.8
prob_mutation = 0.3
num_mutate = 9
iteration = 10000

# Repeat
for i in range(iteration):
    # Selection
    selected_parents = [parent for parent, _ in sorted_scores_dict[:2]]
    #first_parent, second_parent = np.random.choice(selected_parents, 2)
    first_parent, second_parent = selected_parents
    # Crossover
    first_idx = np.sort(np.random.choice(all_idx, size=int(prob_crossover * target_len), replace=False))
    ## second_idx = np.setdiff1d(all_idx, rand_idx)
    offspring = ''.join([first_parent[k] if k in first_idx else second_parent[k] for k in range(0, target_len)])
    ## For holding number of population, using pop and append offspring
    population.pop(0)
    population.append(offspring)
    # Mutation (Scramble Mutation)
    for pop_idx in range(0, num_population):
        tmp_pop = list(population[pop_idx])
        if np.random.random() < prob_mutation:
            mutate_start_point = np.random.randint(0, int(target_len - num_mutate + 1))
            mutate_end_point = mutate_start_point + num_mutate
            tmp_pop_ = tmp_pop[mutate_start_point:mutate_end_point]
            np.random.shuffle(tmp_pop_)
            tmp_pop[mutate_start_point:mutate_end_point] = tmp_pop_
            population[pop_idx] = ''.join(tmp_pop)
        else:
            next
    # Compute fitness
    scores_dict = {candidate : fitness_func(target, candidate)  for candidate in population}
    sorted_scores_dict = sorted(scores_dict.items(), key=operator.itemgetter(1), reverse=True)
    print("\n%s Selected Parents : 1) %s | 2) %s" % (i, first_parent, second_parent))
    print("Offspring : %s" % (offspring))
    print("Top 1 candidate and Score : %s | %.4f \n" % (sorted_scores_dict[0]))
    if sorted_scores_dict[0][1] > 0.99:
        print("End iteration : %1d" % (i))
        print("Best solution : %s | %.4f" % (sorted_scores_dict[0]))
        break
