import argparse
import os
import numpy as np
import tensorflow as tf
from tensorflow import layers

from collections import OrderedDict 
from collections import deque

import copy
import gym
import gym.spaces

from scipy import signal

np.random.seed(0)
tf.set_random_seed(0)

parser = argparse.ArgumentParser()
parser.add_argument("--npop", dest="npop", type=int, default=50, 
        help="Number of populations, element of population is parameter set")
parser.add_argument("--sigma", dest="sigma", type=float, default=0.1, 
        help="Jitter parameter")
parser.add_argument("--alpha", dest="alpha", type=float, default=0.01, 
        help="Learning rate")
parser.add_argument("--gpuid", dest="gpuid", type=str, default=None, 
        help="GPU id")

args = parser.parse_args()
gpuid = "" if args.gpuid is None else args.gpuid
os.environ["CUDA_VISIBLE_DEVICES"] = gpuid

env = gym.make("CartPole-v0")
num_act = 2
npop = args.npop
sigma = args.sigma
alpha = args.alpha
num_episode = 200
max_prob = deque(maxlen=100)

# # Discounting function used to calculate discounted returns.
# def discount(x, gamma=0.99):
#     return signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]

def discount(rewards, dones, gamma=0.99):
    rollings = []
    rolling = 0
    for rew, don in zip(reversed(rewards), reversed(dones)):
        rolling = gamma * rolling * (1 - don) + rew
        rollings.append(rolling)
    rollings.reverse()
    discounted_r = np.array(rollings).reshape(-1,1)
    return discounted_r

# Model
class model(object):
    def __init__(self, sess, name, num_act):
        self.name = name
        self.sess = sess
        with tf.variable_scope(self.name):
            self.inputs_pl = tf.placeholder(tf.float32, shape=[None, 4], name="inputs_pl")
            self.actions_pl = tf.placeholder(tf.int32, name="inputs_pl")
            self.dis_r_pl = tf.placeholder(tf.float32, name="dis_r_pl")

            self.head1 = layers.dense(self.inputs_pl, 60, 
            activation=tf.nn.relu, 
            use_bias=False, 
            kernel_initializer=tf.variance_scaling_initializer, 
            name="head1")

            self.head2 = layers.dense(self.head1, 40, 
            activation=tf.nn.relu, 
            use_bias=False, 
            kernel_initializer=tf.variance_scaling_initializer, 
            name="head2")

            self.out = layers.dense(self.head2, num_act, 
            activation=tf.nn.softmax,
            use_bias=False,
            kernel_initializer=tf.variance_scaling_initializer, 
            name="out")

        self.single_action_prob = tf.reduce_sum(self.out * tf.one_hot(self.actions_pl, depth=num_act), axis=1)
        self.log_action_prob = -tf.log(tf.clip_by_value(self.single_action_prob, 1e-9, 1.0)) * self.dis_r_pl
        self.actor_loss = tf.reduce_sum(self.log_action_prob)

    def init(self):
        init_op = tf.global_variables_initializer()
        return self.sess.run(init_op)

    def reset(self):
        reset_op = tf.reset_default_graph()
        return self.sess.run(reset_op)

    def collect_param(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)

    def forward(self, x):
        feed_dict = {self.inputs_pl : x}
        return self.sess.run(self.out, feed_dict)

def add_noise(sess, v, m, sigma):
    """Add sigma * v's value to m's."""
    return [sess.run(m[i].assign_add(sigma * v[i])) for i in range(len(v))]
        
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

actor = model(sess=sess, name='actor', num_act=num_act)
actor.init()

actor_vars = actor.collect_param()
actor_vars_shape = [var.shape.as_list() for var in actor_vars]

N = [[tf.random_normal(shape=s, mean=0, stddev=1, dtype=tf.float32) for s in actor_vars_shape] for _ in range(npop)]
R = np.zeros(npop)

actor_vars = actor.collect_param()

# Training -------------
for i_ep in range(num_episode):
    actor_vars = actor.collect_param()

    N = [[tf.random_normal(shape=s, mean=0, stddev=1, dtype=tf.float32) for s in actor_vars_shape] for _ in range(npop)]
    R = np.zeros(npop)
    ep_reward_arr = np.zeros(npop)

    for i, p_ in enumerate(N):
        counts = 0
        init_state = env.reset()
        done = False
        _ = add_noise(sess, p_, actor.collect_param(), sigma)
        ep_reward = 0

        states_m = []
        actions_m = []
        rewards_m = []
        dones_m = []

        while not done:
            state = init_state.copy() if counts == 0 else state
            prob = actor.forward(state.reshape(1,-1).astype(np.float32))            
            # action = np.random.choice(np.arange(0, num_act), size=1, p=prob[0])[0]
            action = np.argmax(prob[0])
            
            next_frame, reward, done, _ = env.step(action)

            actions_m.append(action)
            dones_m.append(int(done))
            states_m.append(state)
            state = next_frame
            
            ep_reward += reward
            rewards_m.append(reward)
            counts += 1
            max_prob.append(prob)
        
        states = np.vstack(states_m)
        actions = np.hstack(actions_m)
        rewards = np.hstack(rewards_m)
        dones = np.hstack(dones_m)

        dis_r = discount(rewards, dones)
        dis_r = dis_r.reshape(-1,)

        R[i] = -1 * actor.sess.run(actor.actor_loss, feed_dict={actor.inputs_pl: states, actor.actions_pl: actions, actor.dis_r_pl: dis_r})
        ep_reward_arr[i] = ep_reward

    probs = np.vstack(max_prob)
    p_m = np.round(probs.max(), 3)

    A = (R - R.mean()) / (R.std() + 1e-7)
    A_tf = [tf.stack([tf.constant(A[k], shape=s_, dtype=tf.float32) for k in range(npop)], axis=0) for s_ in actor_vars_shape]

    tmp_N = np.vstack(N)
    params_list = [tf.stack(tmp_N[:, i].tolist(), axis=0) for i in range(len(actor_vars))]

    weighted_params_list = [A_tf[i] * params_list[i] for i in range(len(actor_vars))]

    _ = [var_c.assign_add(alpha / (npop * sigma) * tf.reduce_sum(weighted_params_list[i], axis=0)) for i, var_c in enumerate(actor_vars)]
    
    # alpha = min(alpha * 0.995, 0.0001)
    print("Episode {}, Min Score : {} Max Score : {} Max Prob {} ".format(i_ep, ep_reward_arr.min(), ep_reward_arr.max(), p_m))

# Testing
print("\n\nStart Testing !!")
is_render = False
for i_ep in range(100):
    counts = 0
    init_state = env.reset()
    done = False
    ep_reward = 0
    while not done:
        state = init_state.copy() if counts == 0 else state
        prob = actor.forward(state.reshape(1,-1).astype(np.float32)) 
        # action = np.random.choice(np.arange(0, num_act), size=1, p=prob[0])[0]
        action = np.argmax(prob[0])
        if is_render:
            env.render()
        next_frame, reward, done, _ = env.step(action)
        state = next_frame
        ep_reward += reward
        counts += 1
    print("Episode {}, Score : {} ".format(i_ep, ep_reward))
