# Natural Evolution Strategies(이하 NES) for Mnist
# From : https://gist.github.com/karpathy/77fbb6a8dac5395f1b73e7a89300318d
# From : https://gist.github.com/xmfbit/b27cdbff68870418bdb8cefa86a2d558

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dset # for mnist dataset
import torchvision.transforms as transforms
import copy
from collections import OrderedDict

np.random.seed(0)
torch.random.manual_seed(0)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Network For Mnist
class Net(nn.Module):
    
    def __init__(self):
        super(Net, self).__init__()
        # 1 input image channel, 6 output channels, 5x5 square convolution
        # kernel
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(256, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # Max pooling over a (2, 2) window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        # If the size is a square you can only specify a single number
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

root = "../data"
trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (1.0,))])

train_set = dset.MNIST(root=root, train=True, transform=trans, download=True)
test_set = dset.MNIST(root=root, train=False, transform=trans, download=True)

batch_size = 100

train_loader = torch.utils.data.DataLoader(
                 dataset=train_set,
                 batch_size=batch_size, 
                 shuffle=True)
test_loader = torch.utils.data.DataLoader(
                dataset=test_set,
                batch_size=batch_size,
                shuffle=False)

print('==>>> total trainning batch number: {}'.format(len(train_loader)))
print('==>>> total testing batch number: {}'.format(len(test_loader)))

# 아래 f는 우리가 최적화하고자하는 값을 구해주는 함수를 작성하는 부분이다.
# 본 파일에서는 따로 구현하지 않고 loss에 -1을 곱하여 최대화하는 방향으로
# parameters를 업데이트하도록 했다.
# 그러나 loss에 -1을 곱할 필요없이 그냥 업데이트 방식을 Gradient Ascent가 아닌
# Gradient Descent 방식을 취하면 된다.
# # the function we want to optimize
# def f(w):
#   # here we would normally:
#   # ... 1) create a neural network with weights w
#   # ... 2) run the neural network on the environment for some time
#   # ... 3) sum up and return the total reward

#   # but for the purposes of an example, lets try to minimize
#   # the L2 distance to a specific solution vector. So the highest reward
#   # we can achieve is 0, when the vector w is exactly equal to solution
#   reward = -np.sum(np.square(solution - w))
#   return reward


# hyperparameters
npop = 50 # population size
sigma = 0.1 # noise standard deviation
alpha = 0.001 # learning rate

# start the optimization
net = Net().to(device) # our initial guess is random
net_copy = copy.deepcopy(net)
print(net)

criterion = nn.CrossEntropyLoss()

sigma = 0.01
npop = 50
alpha = 0.01

epochs = 10
for epoch in range(epochs):
      # training
      ave_loss = 0
      
      for batch_idx, (x, target) in enumerate(train_loader):
            
            x = x.to(device)
            target = target.to(device)

            model_keys = net.state_dict().keys()

            # jitter w using gaussian of sigma 0.01
            N = [OrderedDict({key : param + sigma * torch.rand_like(param) for key, param in zip(model_keys, net.parameters())}) for _ in range(npop)]
            R = np.zeros(npop)
            out = net(x)
            loss = criterion(out, target)

            for j in range(npop):
                  net.load_state_dict(N[j])
                  out = net(x)
                  R[j] = -criterion(out, target) # 우리가 최적화하고자하는 값

            # standardize the rewards to have a gaussian distribution
            A = (R - np.mean(R)) / np.std(R)

            ave_loss = ave_loss * 0.9 + loss.data[0] * 0.1
            params_list = [[] for _ in range(len(model_keys))]
            for i in range(npop):
                  for j in range(len(params_list)):
                        params_list[j].append(list(N[i].values())[j].unsqueeze(0) * A[i])

            # parameters update (not backpropagation)
            for i_, param in enumerate(net_copy.parameters()):
                  param.data.add_(alpha / (npop * sigma) * torch.cat(params_list[i_], dim = 0).sum(dim=0))

            net.load_state_dict(net_copy.state_dict())
                            
            if (batch_idx+1) % 100 == 0 or (batch_idx+1) == len(train_loader):
                print('==>>> epoch: {}, batch index: {}, train loss: {:.6f}'.format(
    epoch, batch_idx+1, ave_loss))

# testing
correct_cnt, ave_loss = 0, 0
total_cnt = 0
for batch_idx, (x, target) in enumerate(test_loader):
    x = x.to(device)
    target = target.to(device)
    out = net(x)
    loss = criterion(out, target)
    _, pred_label = torch.max(out.data, 1)
    total_cnt += x.data.size()[0]
    correct_cnt += (pred_label == target.data).sum()
    # smooth average
    ave_loss = ave_loss * 0.9 + loss.data[0] * 0.1
  
print("Test Accuracy : {}".format(correct_cnt.data.cpu().numpy()/ total_cnt))
# 테스트 정확도 : 약 78.47%
# Epochs의 수가 충분치 않았다. (10번)
# 많이 설정해도 되긴하지만, Supervised Learning(이하 SL)에서는 NES가 
# Backpropagation보다 학습속도면에서 많이 느리기 때문에 
# 굳이 NES를 쓰는걸 추천하고 싶지는 않다. RL과 비교할만한 방식이지 SL과는
# 비교할 필요가 없다.
# 이 내용이 궁금하다면 아래 사이트의 Conclusion을 살펴보면 된다.
# https://blog.openai.com/evolution-strategies/