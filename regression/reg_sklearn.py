# This code is about Genetic Algorithm for regression hyperparameters.
# Used data : diabets in datasets
# Used module : sklearn
# Multiple linear regression

from matplotlib import pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

diabetes = datasets.load_diabetes()
diabetes_X = diabetes.data

train_idx = np.random.choice(np.arange(0, diabetes_X.shape[0]), int(0.7 * diabetes_X.shape[0]), replace=False)
test_idx = np.setdiff1d(np.arange(0, diabetes_X.shape[0]), train_idx)

diabetes_X_train = diabetes_X[train_idx]
diabetes_X_test = diabetes_X[test_idx]

diabetes_Y_train = diabetes.target[train_idx]
diabetes_Y_test = diabetes.target[test_idx]

reg_obj = linear_model.LinearRegression(n_jobs=2)

reg_obj.fit(diabetes_X_train, diabetes_Y_train)

diabetes_Y_pred = reg_obj.predict(diabetes_X_test)

print('Coefficients: \n', reg_obj.coef_)
print('Mean squared error: %.2f' \
       % (mean_squared_error(diabetes_Y_test, diabetes_Y_pred)))
print('Variance score: %.2f' % (r2_score(diabetes_Y_test, diabetes_Y_pred)))

plt.scatter(diabetes_Y_test, diabetes_Y_pred, color='blue')
#plt.plot(diabetes_Y_test, diabetes_Y_pred, color='green', linewidth=3)
plt.show()
