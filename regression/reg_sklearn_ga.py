# This code is about Genetic Algorithm for selecting variables.
# Used data : diabets in datasets
# Used module : sklearn
# Support Vector Regression in sklearn.

from matplotlib import pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.ensemble import RandomForestRegressor as RFreg
from sklearn.metrics import mean_squared_error, r2_score
import operator
import os
import sys
from joblib import Parallel, delayed

diabetes = datasets.load_diabetes()
diabetes_X = diabetes.data

train_idx = np.random.choice(np.arange(0, diabetes_X.shape[0]), int(0.7 * diabetes_X.shape[0]), replace=False)
test_idx = np.setdiff1d(np.arange(0, diabetes_X.shape[0]), train_idx)

diabetes_X_train = diabetes_X[train_idx]
diabetes_X_test = diabetes_X[test_idx]
diabetes_Y_train = diabetes.target[train_idx]
diabetes_Y_test = diabetes.target[test_idx]

## GA ##
init_num_population = 200
num_features = 10
len_hp = 2
all_idx = np.arange(0, len_hp) # len_hp : length of each hyperparameter set
num_trees_range = [20, 100]
max_depth_range = [1, 9]

population = [[np.random.randint(num_trees_range[0], num_trees_range[1]), np.random.randint(max_depth_range[0], max_depth_range[1])] 
               for _ in range(init_num_population)]
candidates = population.copy()
# var_ind_list = [np.argwhere(pop == 1).reshape(-1,) for pop in population]
pop_size = 100
iteration = 1000
prob_crossover = 0.8
prob_mutation = 0.05
num_mutate = 1
# normalize=True

def regression_fit(x, y, num_tree=10, max_depth=None):
    X_train = x
    Y_train = y
    reg_ = RFreg(n_estimators=num_tree, max_depth=max_depth)
    reg_.fit(X_train, Y_train)
    return reg_

def fitness_score(y_true, y_pred, N, p):
    """Define fitness of GA. (adjusted R^2)."""
    r2 = r2_score(y_true, y_pred)
    return 1 - (1 - r2) * (N - 1) / (N - p - 1)

n_data = diabetes_Y_test.shape[0]

# Once computing fitness

reg_obj_list = [regression_fit(diabetes_X_train, diabetes_Y_train, num_tree, max_depth) for num_tree, max_depth in population]
diabetes_Y_pred_list = [reg_obj.predict(diabetes_X_test) for reg_obj in reg_obj_list]
scores_list = [fitness_score(diabetes_Y_test, Y_pred, n_data, num_features) for Y_pred in diabetes_Y_pred_list]

for i in range(iteration):
    candidates_probs = np.exp(scores_list) / np.sum(np.exp(scores_list))
    population = []
    for _ in range(0, int(pop_size/2)):
        first_parent = candidates[np.random.choice(np.arange(0, len(candidates)), p=candidates_probs)]
        second_parent = candidates[np.random.choice(np.arange(0, len(candidates)), p=candidates_probs)]
        first_idx = np.sort(np.random.choice(all_idx, size=int(prob_crossover * len_hp), replace=False))
        second_idx = np.setdiff1d(all_idx, first_idx)
        offspring_1 = [first_parent[k] if k in first_idx else second_parent[k] for k in range(0, len_hp)]
        offspring_2 = [second_parent[k] if k in first_idx else first_parent[k] for k in range(0, len_hp)]
        population.append(np.array(offspring_1))
        population.append(np.array(offspring_2))
    for idx in range(0, len(population)):
        tmp_pop = population[idx]
        if np.random.random() < prob_mutation:
            mutate_idx = np.random.choice(np.arange(0, len_hp), num_mutate, replace=False)
            tmp_pop_ = np.array([tmp_pop[k] + np.random.randint(-3, 3) if k in mutate_idx else tmp_pop[k] for k in range(0, len_hp)])
            tmp_pop_ = [np.clip(tmp_pop_[k], num_trees_range[0], num_trees_range[1]) if k == 0 else np.clip(tmp_pop_[k], \
                        max_depth_range[0], max_depth_range[1]) for k in range(0, len_hp)]
            population[idx] = tmp_pop_
        else:
            next
    reg_obj_list = [regression_fit(diabetes_X_train, diabetes_Y_train, num_tree, max_depth) for num_tree, max_depth in population]
    diabetes_Y_pred_list = [reg_obj.predict(diabetes_X_test) for reg_obj in reg_obj_list]
    scores_list = [fitness_score(diabetes_Y_test, Y_pred, n_data, num_features) for Y_pred in diabetes_Y_pred_list]
    candidates = population.copy()
    which_max = np.argmax(scores_list)
    # Top1_Coefficients = reg_obj_list[which_max].coef_
    Top1_score = np.max(scores_list)
    Top1_candidate = candidates[which_max]
    print('\nIteration : %1d' % (i))
    # print('Top 1 Coefficients: \n', Top1_Coefficients)
    print('Top 1 Score: %.4f' % (Top1_score))
    print('Top 1 Candidate: %s' % (Top1_candidate), end="\n")
    if Top1_score > 0.50:
        print("Congratulation!")
        print("End iteration: %1d" % (i))
        print("Best Solution: %s | %.4f" % (Top1_candidate, Top1_score), end="\n")

plt.scatter(diabetes_Y_test, diabetes_Y_pred_list[which_max], color='blue')
plt.plot(diabetes_Y_test, diabetes_Y_test, color='green', linewidth=3)
plt.savefig("top_candidate.png")
plt.show()

