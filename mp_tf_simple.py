import multiprocessing as mp
from functools import partial
import numpy as np
import time
import argparse
import tensorflow as tf
from tensorflow import layers 

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--num_p", dest="num_p", type=int, default=1, 
            help="Number of process")

args = parser.parse_args()

num_p = args.num_p

params = np.array([4., 2.3, -4.1, 2.5])
x_data = np.random.randn(200, 4)
y_data = np.sum(x_data * params, axis=1).reshape(-1,1)
dum_data = np.hstack([x_data, y_data])

def model_fn(data, i):
    import tensorflow as tf
    from tensorflow import layers 
    x_data = data[:, :4]
    y_data = data[:, 4:]
    
    input_pl = tf.placeholder(tf.float32, shape=[None, 4], name="input_pl")
    target_pl = tf.placeholder(tf.float32, shape=[None, 1], name="target_pl")
    
    with tf.variable_scope("model", reuse=tf.AUTO_REUSE):
        h1 = layers.dense(input_pl, 10, activation=tf.nn.relu)
        h2 = layers.dense(h1, 10, activation=tf.nn.relu)
        out = layers.dense(h2, 1, activation=None)

    loss_op = tf.reduce_mean(tf.losses.mean_squared_error(labels=target_pl, predictions=out))

    optim = tf.train.AdamOptimizer(learning_rate=0.01)
    train_op = optim.minimize(loss_op)
    
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()
    sess.run(init)
    epoch = 30
    for _ in range(epoch):
        loss, _ = sess.run([loss_op, train_op], feed_dict={input_pl : x_data, target_pl : y_data})

    return loss

# from joblib import Parallel, delayed
# result = Parallel(n_jobs=num_p)(delayed(model_fn)(dum_data) for _ in range(10))
# print(result)
if __name__ == '__main__':
    start = time.time()
    scores = []
    with mp.Pool(num_p) as p:
        for i, score in enumerate(p.imap(partial(model_fn, dum_data), range(num_p))):
            print(i, score)
            scores.append(score)
    print(scores)
    print(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="model"))
    print("Time taken = {0:5f}".format(time.time() - start))