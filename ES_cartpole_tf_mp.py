import argparse
import os
import numpy as np
import tensorflow as tf
from tensorflow import layers
import time

import multiprocessing as mp
from functools import partial
from collections import deque

import copy
import gym
import gym.spaces

np.random.seed(0)
tf.set_random_seed(0)

gym.logger.set_level(40)

parser = argparse.ArgumentParser()
parser.add_argument("--npop", dest="npop", type=int, default=50, 
        help="Number of populations, element of population is parameter set")
parser.add_argument("--sigma", dest="sigma", type=float, default=0.01, 
        help="Jitter parameter")
parser.add_argument("--alpha", dest="alpha", type=float, default=0.01, 
        help="Learning rate")
parser.add_argument("--gpuid", dest="gpuid", type=str, default=None, 
        help="GPU id")
parser.add_argument("--num_p", dest="num_p", type=int, default=1, 
            help="Number of process")

args = parser.parse_args()

def discount(rewards, dones, gamma=0.99):
    rollings = []
    rolling = 0
    for rew, don in zip(reversed(rewards), reversed(dones)):
        rolling = gamma * rolling * (1 - don) + rew
        rollings.append(rolling)
    rollings.reverse()
    discounted_r = np.array(rollings).reshape(-1,1)
    return discounted_r

# Model
class model(object):
    def __init__(self, sess, name, num_act):
        self.name = name
        self.sess = sess
        with tf.variable_scope(self.name):
            self.inputs_pl = tf.placeholder(tf.float32, shape=[None, 4], name="inputs_pl")
            self.actions_pl = tf.placeholder(tf.int32, name="inputs_pl")
            self.dis_r_pl = tf.placeholder(tf.float32, name="dis_r_pl")

            self.head1 = layers.dense(self.inputs_pl, 60, 
            activation=tf.nn.relu, 
            use_bias=False, 
            kernel_initializer=tf.variance_scaling_initializer, 
            name="head1")

            self.head2 = layers.dense(self.head1, 40, 
            activation=tf.nn.relu, 
            use_bias=False, 
            kernel_initializer=tf.variance_scaling_initializer, 
            name="head2")

            self.out = layers.dense(self.head2, num_act, 
            activation=tf.nn.softmax,
            use_bias=False,
            kernel_initializer=tf.variance_scaling_initializer, 
            name="out")

        self.single_action_prob = tf.reduce_sum(self.out * tf.one_hot(self.actions_pl, depth=num_act), axis=1)
        self.log_action_prob = -tf.log(tf.clip_by_value(self.single_action_prob, 1e-9, 1.0)) * self.dis_r_pl
        self.actor_loss = tf.reduce_sum(self.log_action_prob)

    def init(self):
        init_op = tf.global_variables_initializer()
        return self.sess.run(init_op)

    def reset(self):
        reset_op = tf.reset_default_graph()
        return self.sess.run(reset_op)

    def collect_param(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)

    def forward(self, x):
        feed_dict = {self.inputs_pl : x}
        return self.sess.run(self.out, feed_dict)

gpuid = "" if args.gpuid is None else args.gpuid
os.environ["CUDA_VISIBLE_DEVICES"] = gpuid
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

num_act = 2
npop = args.npop
sigma = args.sigma
alpha = args.alpha
num_episode = 50

def add_noise(sess, v, m, sigma):
    """Add sigma * v's value to m's."""
    return [sess.run(m[i].assign_add(sigma * v[i])) for i in range(len(v))]

def add_params(sess, v, m, alpha, npop, sigma):
    """Add v's value to m's."""
    return [sess.run(var_c.assign_add(alpha / (npop * sigma) * tf.reduce_sum(m[i], axis=0))) for i, var_c in enumerate(v)]

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

actor = model(sess=sess, name='actor', num_act=num_act)
actor.init()

actor_vars = actor.collect_param()
actor_vars_shape = [var.shape.as_list() for var in actor_vars]

def play(npop, args, returns_list, i_ep, i):
    env = gym.make("CartPole-v0")
    N = [[np.random.randn(*s).astype(np.float32) for s in actor_vars_shape] for _ in range(npop)]
    R = np.zeros(npop)
    ep_reward_arr = np.zeros(npop)

    for i, pop in enumerate(N):
        counts = 0
        init_state = env.reset()
        done = False
        _ = add_noise(sess, pop, actor.collect_param(), sigma)
        ep_reward = 0

        states_m = []
        actions_m = []
        rewards_m = []
        dones_m = []

        while not done:
            state = init_state.copy() if counts == 0 else state
            prob = actor.forward(state.reshape(1,-1).astype(np.float32))            
            # action = np.random.choice(np.arange(0, num_act), size=1, p=prob[0])[0]
            action = np.argmax(prob[0])
            
            next_frame, reward, done, _ = env.step(action)

            actions_m.append(action)
            dones_m.append(int(done))
            states_m.append(state)
            state = next_frame
            
            ep_reward += reward
            rewards_m.append(reward)
            counts += 1
            max_prob.append(prob)
        
        states = np.vstack(states_m)
        actions = np.hstack(actions_m)
        rewards = np.hstack(rewards_m)
        dones = np.hstack(dones_m)

        dis_r = discount(rewards, dones)
        dis_r = dis_r.reshape(-1,)

        R[i] = -1 * actor.sess.run(actor.actor_loss, feed_dict={actor.inputs_pl: states, actor.actions_pl: actions, actor.dis_r_pl: dis_r})
        ep_reward_arr[i] = ep_reward
    
    returns_list.append((R, N, max_prob, ep_reward_arr))

num_p = args.num_p
max_prob = deque(maxlen=100)
if __name__ == '__main__':
    manager = mp.Manager()
    for i_ep in range(num_episode):
        start = time.time()
        params_list = manager.list()
        scores = []

        p_pool = mp.Pool(num_p)
        p_result = p_pool.map(partial(play, npop, args, params_list, i_ep), range(num_p))
        p_pool.close()
        p_pool.join()

        time.sleep(1)

        params_list = list(params_list)

        total_npop = num_p * npop
        
        R = []
        N = []
        mprob = []
        EP_reward = []

        _ = [(R.append(params_list[i][0]), N.append(params_list[i][1]), mprob.append(params_list[i][2]), EP_reward.append(params_list[i][3])) for i in range(len(params_list))]

        R = np.hstack(R)
        N = [y for x in N for y in x]
        mprob = [y for x in mprob for y in x]

        N_tmp = np.vstack(N)
        A = np.float32((R - R.mean()) / (R.std() + 1e-7))
        A_tf = [tf.stack([tf.constant(A[k], shape=s_, dtype=tf.float32) for k in range(len(A))], axis=0) for s_ in actor_vars_shape]

        params_v = [np.stack(N_tmp[:, i].tolist(), axis=0) for i in range(len(actor_vars_shape))]
        weighted_params_list = [A_tf[i] * params_v[i] for i in range(len(actor_vars))]

        # _ = [sess.run(var_c.assign_add(alpha / (total_npop * sigma) * tf.reduce_sum(weighted_params_list[i], axis=0))) for i, var_c in enumerate(actor_vars)]
        _ = add_params(sess, actor_vars, weighted_params_list, alpha, total_npop, sigma)
        # _ = copy_params(sess, copy_vars, actor_vars)
        taken_t = np.round(time.time() - start, 3)
        m_p = np.round(np.max(np.vstack(mprob)), 3)
        print("Ep: {}\tTime taken: {}\tPercentile Score: {}\tMax Prob: {}".format(i_ep, taken_t, np.percentile(EP_reward, [0, 5, 10, 25, 35, 50, 65, 75, 90, 95, 100]), m_p))

    print("\n\nStart Testing !!")
    env = gym.make("CartPole-v0")
    is_render = False
    for i_ep in range(100):
        counts = 0
        init_state = env.reset()
        done = False
        ep_reward = 0
        while not done:
            state = init_state.copy() if counts == 0 else state
            probs = actor.forward(state.reshape(1,-1).astype(np.float32))
            action = np.argmax(prob[0])
            if is_render:
                env.render()
            next_frame, reward, done, _ = env.step(action)
            state = next_frame
            ep_reward += reward
            counts += 1
        print("Episode {}, Score : {} ".format(i_ep, ep_reward))
