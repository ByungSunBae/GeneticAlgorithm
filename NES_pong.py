import os
import sys

import numpy as np
import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
import torch.nn.functional as F
from collections import OrderedDict 
from collections import deque
from skimage.color import rgb2gray
from skimage.transform import resize

import copy

import gym
from source import *

# if we can use gpu, set device cuda:0
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")

print(device)

env = gym.make("Pong-v0")
num_act = 2
C, H, W = 4, 84, 84
npop = 100
sigma = 0.01
alpha = 0.1

actor = actor(name = 'actor').to(device)
actor_copy = copy.deepcopy(actor)

# For test
tmp = env.reset()
tmp_pre = get_screen(tmp)
x = torch.from_numpy(np.stack([tmp_pre, tmp_pre, tmp_pre, tmp_pre], axis=1)).to(device)

print("Actor test : {}".format(actor.forward(x)))

num_episode = 10000
max_prob = deque(maxlen=100)
for i_ep in range(num_episode):
    #_ = env.reset()
    #_ = [env.step(1)[0] for _ in range(20)] # no action 20 steps
    #init_states = env.step(1)[0]
    #done = False
    model_keys = actor.state_dict().keys()
    N = [OrderedDict({key : param + sigma * torch.rand_like(param) for key, param in zip(model_keys, actor.parameters())}) for _ in range(npop)]
    R = np.zeros(npop) 
    for i, p_ in enumerate(N):
        counts = 0
        _ = env.reset()
        _ = [env.step(1)[0] for _ in range(20)]
        init_states = [get_screen(env.step(1)[0], size=(84, 84)) for _ in range(4)]
        done = False

        actor.load_state_dict(p_)
        ep_reward = 0
        while not done:
            #states_m = []
            #actions_m = []
            #rewards_m = []
            #next_states_m = []
            #dones_m = []
            states = init_states.copy() if counts == 0 else states
            probs = actor.forward(torch.from_numpy(np.stack(states, axis=1).astype(np.float32)).to(device))            
            action = np.random.choice(np.arange(2, 4), size=1, p=np.float32(probs.cpu().data.numpy()[0, :]))[0]
            #action = np.argmax(probs.cpu().data.numpy()[0, :]) + 2
            next_frame, reward, done, _ = env.step(action)
            #states_m.append(np.vstack(states)[np.newaxis, ...])
            states.pop(0)
            states.append(get_screen(next_frame, size=(84, 84)))
            #next_states_m.append(np.vstack(states)[np.newaxis, ...])
            #actions_m.append(action)
            #rewards_m.append(reward)
            ep_reward += reward
            #dones_m.append(int(done))
            counts += 1
            max_prob.append(probs.cpu().data.numpy()[0, :])
        R[i] = ep_reward
        #print(R)
    A = (R - np.mean(R)) / (np.std(R) + 1e-7)

    params_list = [[] for _ in range(len(model_keys))]
    for i in range(npop):
        for j, key in enumerate(model_keys):
            params_list[j].append(N[i][key].unsqueeze(0) * A[i])

    for k, param in enumerate(actor_copy.parameters()):
        param.data.add_(alpha / (npop * sigma) * torch.cat(params_list[k], dim=0).sum(dim=0))

    actor.load_state_dict(actor_copy.state_dict())
    print("Episode {}, Max Score : {} Max Prob {} ".format(i_ep, np.max(R), np.vstack(max_prob).max()))
