# This code is about simple genetic algorithm
# the basic process for a genetic algorithm
# 1) Initialization
# 2) Evaluation
# 3) Selection
# 4) Crossover
# 5) Mutation
# 6) Repeat

import random
import numpy as np
import string
import operator
import pandas as pd
from datetime import datetime
from matplotlib import pyplot as plt

# generate target
target = "MANNER MAKES MAN"
print("Target is %s" % (target))

str_alphabet = ' ' + string.ascii_uppercase
str_alphabet_list = list(str_alphabet)
target_len = len(target)

def fitness_func(target, candidate):
    """Compute fitness for genetic algorithm

    Args:
        target (string): thing that we want to know.
        candidate (string): generated word.

    Returns:
        float: score to match target and candidate.

    Raises:
        AssertionError: len(target) equal to len(candidate)
    """
    assert len(target) == len(candidate)
    target_list = list(target)
    candidate_list = list(candidate)
    score_ = [np.float32(t_ == c_) for t_, c_ in zip(target_list, candidate_list)]
    score_ = np.array(score_)
    score = np.mean(score_)
    return score

# Start !
# Generate the initial population
init_num_population = 8000
population = [''.join(np.random.choice(str_alphabet_list, len(target)).tolist()) for _ in range(init_num_population)]

# Compute fitness and sorting
scores_dict = {candidate : fitness_func(target, candidate)  for candidate in population}
sorted_scores_dict = dict(sorted(scores_dict.items(), key=operator.itemgetter(1), reverse=True))
all_idx = np.arange(0, target_len)

# Setting hyperparameters
prob_crossover = 0.9
prob_mutation = 0.05
num_mutate = 4
iteration = 10000
pop_size = 1000

iters = []
scores_top1 = []
candidates_top1 = []

# Repeat
for i in range(iteration):
    # Selection
    candidates_by_score = np.array(list(sorted_scores_dict.keys())).reshape(-1,)
    scores_list = list(sorted_scores_dict.values())
    candidates_probs = np.exp(scores_list) / np.sum(np.exp(scores_list))
    # Crossover and Generate new population
    population = []
    for _ in range(0, int(pop_size/2)):
        first_parent = np.random.choice(candidates_by_score, p=candidates_probs)
        second_parent = np.random.choice(candidates_by_score, p=candidates_probs)
        first_idx = np.sort(np.random.choice(all_idx, size=int(prob_crossover * target_len), replace=False))
        second_idx = np.setdiff1d(all_idx, first_idx)
        offspring_1 = ''.join([first_parent[k] if k in first_idx else second_parent[k] for k in range(0, target_len)])
        offspring_2 = ''.join([second_parent[k] if k in second_idx else first_parent[k] for k in range(0, target_len)])
        population.append(offspring_1)
        population.append(offspring_2)
    # Mutation (Uniform Mutation)
    prob_mutation = max(0.999 * prob_mutation, 0.1)
    for pop_idx in range(0, pop_size):
        tmp_pop = list(population[pop_idx])
        if np.random.random() < prob_mutation:
            #mutate_start_point = np.random.randint(0, int(target_len - num_mutate + 1))
            #mutate_end_point = mutate_start_point + num_mutate
            mutate_idx = np.random.choice(np.arange(0, target_len), num_mutate)
            tmp_pop_ = [random.choice(str_alphabet_list) if idx in mutate_idx else tmp_pop[idx] for idx in range(0, target_len)]
            #tmp_pop_ = tmp_pop[mutate_start_point:mutate_end_point]
            #np.random.shuffle(tmp_pop_)
            #tmp_pop[mutate_start_point:mutate_end_point] = tmp_pop_
            population[pop_idx] = ''.join(tmp_pop_)
        else:
            next
    # Compute fitness
    scores_dict = {candidate : fitness_func(target, candidate)  for candidate in population}
    sorted_scores_dict = sorted(scores_dict.items(), key=operator.itemgetter(1), reverse=True)
    print("Iteration : %1d" % (i))
    print("-------------------Target : %s" % (target))
    print("Top 5 candidate and Score : %s | %.4f \n" % (sorted_scores_dict[0]))
    iters.append(i)
    scores_top1.append(sorted_scores_dict[0][1])
    candidates_top1.append(sorted_scores_dict[0][0])
    if sorted_scores_dict[0][1] > 0.99:
        print("Congratulation!\n")
        print("End iteration : %1d" % (i))
        print("Best solution : %s | %.4f" % (sorted_scores_dict[0]))
        break
    sorted_scores_dict = dict(sorted_scores_dict)

save_log = pd.DataFrame({"Iters": iters, "Scores": scores_top1, "Candidates": candidates_top1})
curr_time = datetime.now()
curr_time = curr_time.strftime("%Y-%m-%d")
save_log.to_csv("./log_{}.csv".format(curr_time))

plt.plot(iters, scores_top1)
plt.ylabel("Scores")
plt.savefig('./log.png')
